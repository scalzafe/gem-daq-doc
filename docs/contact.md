# Contact us!

The majority of communication is done via two Mattermost teams:

-   [CMS GEM DAQ team](https://mattermost.web.cern.ch/cms-gem-daq)
    -   This team has channels that are mainly related to software and
        firmware development, and infrastructure setup
-   [CMS GEM Ops team](https://mattermost.web.cern.ch/cms-gem-ops)
    -   This team has channels that are mainly dedicated to operational
        issues with different
        `teststands <gemos-teststand-guide>`{.interpreted-text
        role="ref"} and CMS P5.

For urgent issues, the GEM DAQ on-call can be reached at (162108), but
note that support is triaged

1.  P5 operations and support
2.  904 QC/testbeam operations and support
3.  904 teststand support
4.  Remote teststand support
