# OptoHybrid

The OptoHybrid (OH) serves several purposes in the GEM front-end system.
It concentrates the trigger signals from all VFATs connected to a given
GEB, and links to the back-end via optical links. The optical link data
transmission happens via the GBTx chip with the GBT protocol, and is
managed via a special ASIC for slow control.

## OptoHybrid FPGA

Unlike the OHv2b, the OHv3 FPGA is not responsible for slow control or
data transfer of tracking data from the VFATs. The OHv3 FPGA deals only
with sending the VFAT trigger data to the CSC OTMB and GEM CTP7.

### Programming the OH FPGA

Programming the OH FPGA uses the "PROM-less" or `BLASTER(tm)` method.
The procedure is:

1.  Power the LV,

2.  Program all GBTs on the OHv3 via one of the methods under
    `expertguide:gemos-gbt-programming`{.interpreted-text role="ref"},

    !!! note
        In the rare case that the GBTx's on your OH are fully fused,
        proceed to step 3

3.  Issue an sca reset following the instructions under
    `gemos-sca-reset`{.interpreted-text role="ref"},

4.  Using `gem_reg.py` from the DAQ PC, connect to the CTP7 of interest
    with `connect eagleXX` and then enable the TTC generator

    ``` bash
    write GEM_AMC.TTC.GENERATOR.ENABLE 1
    ```

5.  Using `gem_reg.py` send a single TTC hard reset to program the FPGA
    with the `BLASTER(tm)`

    ``` bash
    write GEM_AMC.TTC.GENERATOR.SINGLE_HARD_RESET 1
    ```

    !!! warning
        This will issue this reset to *all* OptoHybrids connected to this
        CTP7, which *will* stop any existing data taking, crash any scans,
        and wipe out any present configuration

6.  Check that the FW is loaded into all OptoHybrids present by
    following instructions at
    `gemos-check-fw-versions`{.interpreted-text role="ref"},

    !!! note
        If you see that the FW is not loaded on any of the OptoHybrids of
        interest (or you were expecting a different OH FW version) it is
        likely that either the OH FW is *not* loaded into the CTP7 RAM or
        that a different version of OH FW is loaded into the CTP7 RAM. To
        resolve this login to the CTP7 and execute:

        ``` bash
        cd /mnt/persistent/gemdaq/gemloader
        ./gemloader_configure.sh
        ```

        !!! important
            Sometimes the OH FW does not load successfully into the CTP7 RAM and
            the call of `gemloader_configure.sh` must be repeated several times.

        Then repeat step 5.

        -   If after this the FW is still not loading onto one or more
            OptoHybrids check to make sure you have communication with the
            SCA of interest by following instructions under Section
            `gemos-sca-status`{.interpreted-text role="ref"}.
        -   If the SCA communication is good and the FW is still not
            loading, double check that the TTC generator is enabled by
            reading the value of `GEM_AMC.TTC.GENERATOR.ENABLE`.
        -   If the TTC Generator is enabled, the SCA status is good, and the
            OH FW is in the CTP7 RAM, check to make sure GBT0 is still good,
            see `gemos-gbt-ready-registers`{.interpreted-text role="ref"}.
        -   If GBT0 is no longer good then programming the FPGA will not be
            possible (as this is through GBT0).
        -   In this case you may need to start the procedure again from
            step 1.
        -   One final check would be to ensure the CTP7 mapping register has
            the correct value, see
            `gemos-slow-control-ctp7-mapping`{.interpreted-text role="ref"}.
        -   If after all these you are still *unable* to program the FPGA,
            the Linux image of your CTP7 may be to old, contact the DAQ
            expert Although typically this is not the case.

        Failure to program the FPGA is usually a result of:

        1.  Hardware problem,
        2.  Failure to execute the procedure in the correct order

7.  Finally, using `gem_reg.py` disable the TTC generator

    ``` bash
    write GEM_AMC.TTC.GENERATOR.ENABLE 0
    ```

    !!! important
        While the TTC generator is enabled the CTP7 will *ignore* all TTC
        commands from the backplane

### Checking trigger link status

To check the status of the OH-CTP7 trigger link for `OHY` execute:

``` sh
kw GEM_AMC.TRIGGER.OHY.LINK
```

Where `Y` is an integer representing the OH number. A healthy link
should come back as:

``` sh
eagleXX > kw GEM_AMC.TRIGGER.OH0.LINK
0x66000e80 r    GEM_AMC.TRIGGER.OH0.LINK0_SBIT_OVERFLOW_CNT             0x00000000
0x66000e80 r    GEM_AMC.TRIGGER.OH0.LINK1_SBIT_OVERFLOW_CNT             0x00000000
0x66000e84 r    GEM_AMC.TRIGGER.OH0.LINK0_MISSED_COMMA_CNT              0x00000000
0x66000e84 r    GEM_AMC.TRIGGER.OH0.LINK1_MISSED_COMMA_CNT              0x00000000
0x66000e8c r    GEM_AMC.TRIGGER.OH0.LINK0_OVERFLOW_CNT                  0x00000000
0x66000e8c r    GEM_AMC.TRIGGER.OH0.LINK1_OVERFLOW_CNT                  0x00000000
0x66000e90 r    GEM_AMC.TRIGGER.OH0.LINK0_UNDERFLOW_CNT                 0x00000000
0x66000e90 r    GEM_AMC.TRIGGER.OH0.LINK1_UNDERFLOW_CNT                 0x00000000
0x66000e94 r    GEM_AMC.TRIGGER.OH0.LINK0_SYNC_WORD_CNT                 0x00000000
0x66000e94 r    GEM_AMC.TRIGGER.OH0.LINK1_SYNC_WORD_CNT                 0x00000000
```

If your link does not look like the above the link is not healthy. First
try reseting the counters and then reading them again by executing:

``` sh
write GEM_AMC.TRIGGER.CTRL.CNT_RESET  1
kw GEM_AMC.TRIGGER.OHY.LINK
```

If your link still does not match the example above try the following:

1.  If the trigger fiber is accessible as a stand alone fiber (e.g., not
    in an MTP12 bundle) check that there is red light in both ends of
    the fiber coming from the OHv3. If so issue a reset, if not
    -   the board may not be on,
    -   the fiber may be faulty,
    -   the VTTx may be faulty, or
    -   the VTTx may not be receiving the correct voltage, check that
        the 2.5V pin on the OH; with no load it should be between
        \[2.45, 2.66\]V.
2.  Try reloading the firmware to the OHv3 by following instructions
    under Section `gemos-optohybrid-programming`{.interpreted-text
    role="ref"}, in rare cases the trigger block of the OH FW does not
    start properly.

### Masking VFATs from trigger

You can write a 24 bit mask to `GEM_AMC.OH.OHX.FPGA.TRIG.CTRL.VFAT_MASK`
to mask a given set of VFATs from the trigger, having a 1 in the
$N^{th}$ bit means the $N^{th}$ VFAT will be masked.

### Temperature monitoring

The FPGA core temperature is accessible from the sysmon registers in the
OHv3 address table, and there are nine additional PT100(0) sensors
located around the board. These PT100(0) sensors are read by the SCA
when monitoring is enabled, see
`gemos-sca-pt100-sensors`{.interpreted-text role="ref"}. The SCA gives
output in ADC counts. For the details of how the conversions for
temperature and voltages are done see
`gemos-frontend-sca-conversion`{.interpreted-text role="ref"}

## GBTx

The
[GBTx](http://iopscience.iop.org/article/10.1088/1748-0221/10/03/C03034/meta)
is a radiation hard gigabit transceiver for optical links, providing
simultaneous transfer of readout data, timing, and trigger signals, as
well as slow control and monitoring information.

### E-link assignment

A correspondence between the VFAT position identifiers (SW vs. HW), the
associated e-link, and the controlling GBTx is shown below.

=== "GE1/1"

    For the GE1/1 OptoHybrid v3 (any version) the correspondence is
    
      ------------------------------------------------
      VFAT Pos (SW)   VFAT Pos (HW)   GBTx   E-Link
      --------------- --------------- ------ ---------
      0               24              1      5
    
      1               23              1      9
    
      2               22              1      2
    
      3               21              1      3
    
      4               20              1      1
    
      5               19              1      8
    
      6               18              1      6
    
      7               17              0      6
    
      8               16              1      4
    
      9               15              2      1
    
      10              14              2      5
    
      11              13              2      4
    
      12              12              0      3
    
      13              11              0      2
    
      14              10              0      1
    
      15              9               0      0
    
      16              8               1      7
    
      17              7               2      8
    
      18              6               2      6
    
      19              5               2      7
    
      20              4               2      2
    
      21              3               2      3
    
      22              2               2      9
    
      23              1               0      8
      ------------------------------------------------
    
    !!! note
        GBTx0 doesn't use all its e-links for VFAT communication as it is also
        responsible for SCA & FPGA communication on the OptoHybrid.

=== "GE2/1"

    For the GE2/1 OptoHybrid v1(v2)
    
      ------------------------------------------------
      VFAT Pos (SW)   VFAT Pos (HW)   GBTx   E-Link
      --------------- --------------- ------ ---------
      XX              YY              A      B
    
      ------------------------------------------------

=== "ME0"

    For the ME0 OptoHybrid
    
      ------------------------------------------------
      VFAT Pos (SW)   VFAT Pos (HW)   GBTx   E-Link
      --------------- --------------- ------ ---------
      XX              YY              A      B
    
      ------------------------------------------------

### Performing a GBT phase scan

The GBTx must be at least minimally fused (so that it locks to the fiber
link) and the `I2C` jumper for the GBTx in question must *not* be in
place (e.g., open circuit). Before proceeding please check that the GBTx
communication is good by following
`these instructions<gemos-gbt-ready-registers>`{.interpreted-text
role="ref"} to check the GBTx status on a given OH. Once communication
is enabled exectue the following procedure:

``` bash
gbt.py Y X v3b-phase-scan <config file> 2>&1 | tee $HOME/oh_Y_gbt_X_phase_scan.txt
```

This will scan all phases for all e-links on this GBTx and report
whether the phase is good (bad) if the `SYNC_ERR_CNT` of the VFAT on
that e-link is `0x1` (`0x0`).

!!! note
    While the above says `v3b-phase-scan` it is good for any v3 OptoHybrid
    version.

The GBT config files a CTP7 can be found under:

``` bash
/mnt/persistent/gemdaq/gbt
```

### `writeGBTPhase.py`: Manually writing the GBT e-link phase for a given VFAT

You can write the GBT e-link phase for a given VFAT or all VFATs using
the `writeGBTPhase.py` tool by calling from the DAQ machine:

``` bash
writeGBTPhase.py -h
```

which should produce output:

``` bash
usage: writeGBTPhase.py [-h] {single,all} ...

Tool for writing GBT phase for a single or all elink

positional arguments:
  {single,all}  Available subcommands and their descriptions.To view the sub
                menu call writeGBTPhase.py COMMAND -h e.g.
                writeGBTPhase.py single -h
    single      write GBT phase for single VFAT
    all         write GBT phase for all VFAT

optional arguments:
  -h, --help    show this help message and exit
```

There are two sub-commands `single` and `all`.

=== "`single`"

    ``` bash
    $ writeGBTPhase.py single -h
    usage: writeGBTPhase.py single [-h] shelf slot vfat phase link

    positional arguments:
      shelf       μTCA shelf number
      slot        AMC slot number in the μTCA shelf
      vfat        VFAT number on the OH
      phase       GBT Phase Value to Write
      link        OH number on the AMC

    optional arguments:
      -h, --help  show this help message and exit
    ```

    If you want to write the phase for single VFAT:

    ``` bash
    writeGBTPhase.py single 1 6 23 7 3
    ```

    This will write the phase 7 to VFAT23 on `(shelf,slot,link) = (1,6,3)`.

=== "`all`"

    ``` bash
    $ writeGBTPhase.py all -h
    usage: writeGBTPhase.py all [-h] shelf slot gbtPhaseFile

    positional arguments:
      shelf         μTCA shelf number
      slot          AMC slot number in the μTCA shelf
      gbtPhaseFile  File having link, vfat and phase info.
                    The input file will look like:
                    --------------------------
                    link/i:vfatN/i:GBTPhase/i:
                    4    0    7
                    4    1    9
                    4    2    13
                    --------------------------

    optional arguments:
      -h, --help    show this help message and exit
    ```

    If you want to write a the phases for all VFATs using the input text
    file as expected by the script:

    ``` bash
    writeGBTPhase.py all 1 6 $DATA_PATH/GE11-X-S-INDIA-0015/gbtPhaseSetPoints_GE11-X-S-INDIA-0015_current.log
    ```

    Here, we assumed that we are reading the detector `GE11-X-S-INDIA-0015`.
    This will write phases for all VFATs on `(shelf, slot) = (1, 6)` for the
    link told by the text file.

### GBT_READY registers

There are a set of registers for each optohybrid in the CTP7 FW that
provide information about the GBTx status. To read these reigsters for
the X\^th optohybrid from `gem_reg.py` execute:

``` bash
kw OH_LINKS.OHX.GBT
```

For example a healthy set of GBTx chips would have the GBT ready
register as `0x1` and all the error registers as `0x0`:

``` bash
eagle60 > kw OH_LINKS.OH1.GBT
0x65800800 r    GEM_AMC.OH_LINKS.OH1.GBT0_READY                         0x00000001
0x65800800 r    GEM_AMC.OH_LINKS.OH1.GBT1_READY                         0x00000001
0x65800800 r    GEM_AMC.OH_LINKS.OH1.GBT2_READY                         0x00000001
0x65800800 r    GEM_AMC.OH_LINKS.OH1.GBT0_WAS_NOT_READY                 0x00000000
0x65800800 r    GEM_AMC.OH_LINKS.OH1.GBT1_WAS_NOT_READY                 0x00000000
0x65800800 r    GEM_AMC.OH_LINKS.OH1.GBT2_WAS_NOT_READY                 0x00000000
0x65800800 r    GEM_AMC.OH_LINKS.OH1.GBT0_RX_HAD_OVERFLOW               0x00000000
0x65800800 r    GEM_AMC.OH_LINKS.OH1.GBT1_RX_HAD_OVERFLOW               0x00000000
0x65800800 r    GEM_AMC.OH_LINKS.OH1.GBT2_RX_HAD_OVERFLOW               0x00000000
0x65800800 r    GEM_AMC.OH_LINKS.OH1.GBT0_RX_HAD_UNDERFLOW              0x00000000
0x65800800 r    GEM_AMC.OH_LINKS.OH1.GBT1_RX_HAD_UNDERFLOW              0x00000000
0x65800800 r    GEM_AMC.OH_LINKS.OH1.GBT2_RX_HAD_UNDERFLOW              0x00000000
```

!!! warning
    If `GBTY_READY` is not `0x1` or `GBTY_WAS_NOT_READY` stays `0x1` after
    `gemos-gbt-link-reset`{.interpreted-text role="ref"} then your
    communication is probably *not* good. Check the following:

    -   The electronics are powered,
    -   The TX from the CTP7 to the GBTx is going into the left position (OH
        is oriented with FPGA facing you and VTTx/VTRx are pointing towards
        the floor) of the VTRx that corresponds to this GBTx, or
    -   The TX from the GBTx to the CTP7 makes it to the CTP7 fiber patch
        panel.

### Issuing a GBT link reset

To reset the GBT links and send the VFAT synchronization command
execute:

``` bash
write GEM_AMC.GEM_SYSTEM.CTRL.LINK_RESET 0x1
```

If your GBTx communication is stable this will reset the following
registers to `0x0`:

-   `GBTY_WAS_NOT_READY`,
-   `GBTY_RX_HAD_OVERFLOW`,
-   `GBTY_RX_HAD_UNDERFLOW`, and
-   `SYNC_ERR_CNT`.

This will be applied to all optohybrids and VFATs on the CTP7.

## Slow Control ASIC (SCA)

### The `sca.py` tool

The `sca.py` is a command line tool for sending a variety of commands to
the SCA. For a description of the possible commands and their calling
syntax execute `sca.py -h` for more information.

### Issuing an SCA reset

To issue an SCA reset execute the following from the DAQ PC:

``` bash
sca.py r cardName ohMask
```

For example:

``` bash
sca.py r eagle60 0x3
```

This will issue an SCA reset to OH's 0 and 1 on `eagle60`.

If a red error message appears for one or more of the OH's in your
`ohMask` re-issue the SCA reset until no red error messages appear. For
subsequent SCA resets issued in this way you can use the same `ohMask`
or modify it to remove the healthy OH's. If continuing to issue an sca
reset does not resolve the issue (i.e., red error messages continue to
appear) there is a problem and you probably lost communication. In this
case check the status of `GBT0` on each of the problem GBTs using
instructions under `gemos-gbt-ready-registers`{.interpreted-text
role="ref"}, if GBTX is either not ready or was not ready you may need
to either issue a GBTx link reset (see
`gemos-gbt-link-reset`{.interpreted-text role="ref"}), re-program GBT0
(see `expertguide:gemos-gbt-programming`{.interpreted-text role="ref"}),
or in rare cases power cycle and start from scratch.

#### Checking the SCA status

There are two registers of great importance for checking SCA
communication. They are:

``` bash
GEM_AMC.SLOW_CONTROL.SCA.STATUS.READY
GEM_AMC.SLOW_CONTROL.SCA.STATUS.CRITICAL_ERROR
```

Each is a 32-bit register where the lowest 12 map to the 12 OptoHybrids,
with the $N^{th}$ bit corresponding to the $N^{th}$ OptoHybrid. In the
case of `READY` if the $N^{th}$ bit is high (1) it means the link is
ready and communication is most likely stable (although in some cases
the `READY` bit for a given OptoHybrid is 1 but slow control is not
possible). In the case of `CRITICAL_ERROR` if the $N^{th}$ bit is high
(1) it means the SCA controller on the $N^{th}$ OptoHybrid has
encountered a critical error and needs an SCA reset.

#### Using `amc_info_uhal.py`

You can get the SCA status on all optohybrids on a CTP7 from
`amc_info_uhal.py` command. Execute:

``` bash
amc_info_uhal.py --shelf=X -sY
```

The relevant SCA output for all OptoHybrids connected to the CTP7 in
slot `Y` of shelf `X` will look like:

``` bash
--=======================================--
-> GEM SYSTEM SCA INFORMATION
--=======================================--

READY             0x000003fc
CRITICAL_ERROR    0x00000000
NOT_READY_CNT_OH00 0x00000001
NOT_READY_CNT_OH01 0x00000001
NOT_READY_CNT_OH02 0x00000002
NOT_READY_CNT_OH03 0x00000002
NOT_READY_CNT_OH04 0x00000002
NOT_READY_CNT_OH05 0x00000002
NOT_READY_CNT_OH06 0x00000002
NOT_READY_CNT_OH07 0x00000002
NOT_READY_CNT_OH08 0x00000002
NOT_READY_CNT_OH09 0x00000002
NOT_READY_CNT_OH10 0x00000001
NOT_READY_CNT_OH11 0x00000001
```

!!! note
    The `ipbus` service must be running on the CTP7, otherwise the command
    above will fail.

#### Using `gem_reg.py`

You can get the SCA status on all optohybrids on a CTP7 from
`gem_reg.py` using the following command, with example output:

``` bash
eagleXX > rwc SCA*READY
0x66c00400 r    GEM_AMC.SLOW_CONTROL.SCA.STATUS.READY                   0x00000002
0x66c00408 r    GEM_AMC.SLOW_CONTROL.SCA.STATUS.NOT_READY_CNT_OH0       0x00000001
0x66c0040c r    GEM_AMC.SLOW_CONTROL.SCA.STATUS.NOT_READY_CNT_OH1       0x00000002
0x66c00410 r    GEM_AMC.SLOW_CONTROL.SCA.STATUS.NOT_READY_CNT_OH2       0x00000001
0x66c00414 r    GEM_AMC.SLOW_CONTROL.SCA.STATUS.NOT_READY_CNT_OH3       0x00000001
eagle60 > read GEM_AMC.SLOW_CONTROL.SCA.STATUS.CRITICAL_ERROR
0x66c00404 r    GEM_AMC.SLOW_CONTROL.SCA.STATUS.CRITICAL_ERROR          0x00000000
```

Here we see that SCA READY is `0x2` so only OH1 has stable communication
and no links are in error (critical error is `0x0`).

!!! note
    The `rpcsvc` must be running on the CTP7, otherwise the command above
    will fail.

## ADC counts to voltage conversion

The output values are 12 bit ADC and the voltage value varies from 0 to
1 V. Thus, each bit corresponds to
$\frac{1\mathrm{V}}{2^{12}} \approx 0.244 \mathrm{mV}$.

### PT100/PT1000 Sensors

Mounted at several locations on the OptoHybrid PCB are several PT1000
(PT100 in earlier versions) resistors. These resistors have a well-known
dependence on temperature, and are therefore a good way to measure
temperatures.

#### Example conversion

-   Assume an ADC reading of 44 counts
-   The corresponding voltage =
    $44 \times 0.244 \mathrm{mV} = 10.736 \mathrm{mV}$
-   The PT100 measures temperature using a platinum resistor through
    which $100 \mu A$ of current passes
-   Resistance = $V/I = 10.736\mathrm{mV} / 100 \mu A = 107.36 \Omega$
-   Check the temperature corresponding to the $107.36 \Omega$
    resistance in the [R\--T table](https://www.intech.co.nz/products/temperature/typert/RTD-Pt100-Conversion.pdf).
    This corresponds to $19 C$.

### SCA internal temperature

-   ADC count = 2743
-   Corresponding voltage =
    $2743 \times 0.244 \mathrm{mV} = 669.68 \mathrm{mV}$
-   Now look at the V-T graph. The voltage of 669.68 corresponds to
    \~$25 C$.

[![SCA V-T graph](https://user-images.githubusercontent.com/5220316/58358921-32d1c500-7e81-11e9-8ae6-ae5c3a6805df.png)](https://user-images.githubusercontent.com/5220316/58358921-32d1c500-7e81-11e9-8ae6-ae5c3a6805df.png)

The above V-T graph is taken from page 52 of the [GBT-SCA manual](https://espace.cern.ch/GBT-Project/GBT-SCA/Manuals/GBT-SCA-UserManual.pdf).

### Conversion of ADC for voltage sensors

-   ADC count = 1025
-   A multiplication factor of 4 is used, due to the voltage divider
    circuit having values $3k\Omega$ and $1k\Omega$, respectively.

[![OptoHybrid SCA sensor circuit diagram](https://user-images.githubusercontent.com/5220316/58358934-48df8580-7e81-11e9-83c6-d29534acf581.png)](https://user-images.githubusercontent.com/5220316/58358934-48df8580-7e81-11e9-83c6-d29534acf581.png)

-   Voltage =
    $1025 \times 0.244 \times 4 \mathrm{mV} \approx 1\mathrm{V}$
