# CTP7

For a generic overview of the CTP7 see
`userguide:gemos-ctp7-guide`{.interpreted-text role="ref"}. In the pages
linked below, you will find various expert level documentation to set up
and configure the CTP7, as well as various hints to debug and resolve
various issues.

## Launching a pyhton script with `gempy` on the AMC

To launch a script that needs to access the registers through gempy, that is installed on the BE, one can use the command line in this way:

``` bash
ssh gempro@gem-shelfXX-amcYY "bash -l -c 'python -'" < <path-to-my-python-script>
```

## In-depth CTP7 overview

The CTP7 runs a special 32-bit version of linux located on a 32GB flash
SD card inserted on the AMC. This is run by a 32-bit processor called a
Zynq processor and features extremely fast register access to the Virtex
7 FPGA. The linux OS is loaded from an image on the card at each
boot/reboot and only those files found under:

``` bash
/mnt/persistent
```

are persistent across reboots.

!!! important
    An important caveat is that the `root` password of the CTP7 is the same
    on *all* CTP7\'s. If you are the sysadmin of a given test stand *do not*
    share this with general users, as this will enable them to have `root`
    privileges on any test stand (including P5).

Each CTP7 has an associated, hard-coded, \"birdname\", e.g., `eagle64`.
In addition, when configured to receive a configuration from the UW
`sysmger`, each card will receive a second IP address via DHCP. This
second IP address is given the geographic hostname, e.g.,
`gem-shelfXX-amcYY`

### UW `sysmgr`

The UW system manager (`sysmgr`) is an application running on a DAQ PC
that communicates directly with the CTP7 via IPMI. It provides DNS, time
server, remote logging, and DHCP between the PC and any managed cards.
Configuration of the PC to serve as the `sysmgr` host can be done via
the
:cpp`setupMachine.sh script<gemctp7user:setup_machine>`{.interpreted-text
role="func"}.

### CTP7 filesystem

On the CTP7 there will be several important directories:

``` bash
/mnt/image-persist
/mnt/persistent/config
/mnt/persistent/gemdaq
/mnt/persistent/gemuser
/mnt/persistent/rpcmodules
```

Most are described below, but the `rpcmodules` subdirectory is described
in `gemos-ctp7-lmdb`{.interpreted-text role="ref"}.

#### `/mnt/image-persist`

This directory contains static information related to the Linux image
itself, e.g., if you want to know the build ID of the booted Linux
image:

``` bash
$ cat /mnt/image-persist/build_id
CTP7-GENERIC-20181120T115721-0600-0cdfd2c
```

Here you will also find the tarball of the associated Linux \"Peta
stage\", which is used when compiling libraries and executables to run
directly on the CTP7.

#### `/mnt/persistent/config`

The files in this directory allow you to modify certain configuration
parameters of the Linux image to persist across reboots. Namely, here
you will be able to ensure that newly created users/groups will persist,

-   `root.authorized_keys`

    Adding an `ssh` public key here will enable passwordless login as
    the `root` user.

-   `rpcsvc.acl`

    Add subnet domains here to allow RPC connections from the necessary
    machines. If you run the CTP7s on a `192.168.0.0/16` subnet, you may
    have the following entries:

    ``` bash
    $ cat /mnt/persistent/config/rpcsvc.acl
    ```

    which should produce output:

    ``` bash
    # This file contains the list of IP addresses permitted to access rpcsvc.
    # Each line contains an IP address in CIDR format.
    # Comments begin with # at any point.
    #
    # If you aren't familiar with CIDR, use /32 as the prefix length.
    #
    # Don't remove 127.0.0.1.
    127.0.0.1/32 # Local clients
    192.168.0.0/16 # Lab private networks
    ```

    You should configure this to lock down the access if the network you
    operate the CTP7s on is not completely under your control.

#### `/mnt/persistent/gemdaq`

The `gemdaq` subdirectory looks like:

``` bash
drwxrwxrwx    2 root     root          4096 Aug 10 12:44 address_table.mdb
drwxr-sr-x    5 gemuser  1001          4096 Jun  1  2017 apps
drwxr-xr-x    2 51446    1399          4096 Aug  8 15:45 bin
drwxr-xr-x    2 51446    1399          4096 Aug  8 15:45 fw
drwxr-xr-x    5 root     root          4096 Aug 10 09:24 gbt
drwxrwxr-x    2 51446    1399          4096 Aug  8 15:45 gemloader
drwxr-xr-x    2 51446    1399          4096 Aug  8 15:45 lib
drwxr-xr-x    2 51446    1399          4096 Aug  8 15:41 oh_fw
drwxr-xr-x    3 51446    1399          4096 Aug  8 15:45 python
drwxr-xr-x    3 51446    1399          4096 Aug  8 15:41 scripts
drwxrwxrwx    2 51446    1399         12288 Aug 21 14:01 vfat3
drwxr-xr-x    2 51446    1399          4096 Aug  8 15:46 xml
```

The contents of the most relevant of these subdirectories is described
in more detail below.

-   `address_table.mdb`

    The Lightning in Memory Database (LMDB) will be found under the
    `address_table.mdb` folder, along with a lock file to prevent
    simultaneous access.

    !!! note
        The `address_table.mdb` folder and its contents must have read/write
        permissions to *everyone* or else LMDB related actions *will* fail.

-   `fw`

    The CTP7 firmware will be found under `fw` folder and a set of
    symlinks will be specified there, for example:

    ``` bash
    lrwxrwxrwx    1 51446    1399            23 Aug  8 15:45 gem_ctp7.bit -> gem_ctp7_v3_5_3_4oh.bit
    -rw-r--r--    1 51446    1399      28734919 Aug  3 17:37 gem_ctp7_v3_5_3_4oh.bit
    ```

-   `gbt`

    The `GBTx` configuration files for programming over the fiber link
    will be found under the `gbt` folder and in relevant subfolders:

    ``` bash
    drwxr-xr-x    2 root     root          4096 Aug 10 09:24 OHv3a
    drwxr-xr-x    2 root     root          4096 Aug 10 09:24 OHv3b
    drwxr-xr-x    2 root     root          4096 Aug 10 14:31 OHv3c
    ```

-   `gemloader`

    Configuring the `gemloader` for `BLASTER(tm)` configuration method
    is possible with the `gemloader_configure.sh` script, which is found
    under the `gemloader` subdirectory. The `gemloader` itself is a
    system installed executable, e.g.:

    ``` bash
    which gemloader
    ```

    which should produce output:

    ``` bash
    /bin/gemloader
    ```

-   `lib`

    The `lib` folder has a set of shared object libraries installed that
    are necessary for atomic transactions and logging.

-   `oh_fw`

    The OptoHybrid firmware will be found under `oh_fw` and a set of
    symlinks will be specified there, for example:

    ``` bash
    lrwxrwxrwx    1 51446    1399            22 Aug  8 15:41 optohybrid_top.bit -> optohybrid_3.1.2.B.bit
    lrwxrwxrwx    1 51446    1399            22 Aug  8 15:41 optohybrid_top.mcs -> optohybrid_3.1.2.B.mcs
    -rwxr-xr-x    1 gemuser  1001       5465091 Jun  1  2017 optohybrid_top_2.2.d.fb.bit
    -rwxr-xr-x    1 gemuser  1001      15030033 Jun  1  2017 optohybrid_top_2.2.d.fb.mcs
    ```

-   `python`

    The `python` folder contains several register interface scripts
    specifically the `gbt.py`, `sca.py`, and `reg_interface.py` scripts
    that can be used on the CTP7. Except for `gbt.py` these scripts are
    typically used from the corresponding versions on the DAQ PC.

-   `scripts`

    The `scripts` directory has a series of scripts that are in the
    `$PATH` that enable actions like reloading the CTP7 firmware or
    starting `ipbus` (just to name a few examples).

-   `vfat3`

    The `vfat3` directory for the time being contains the per
    `(ohN,vfatN)` configuration file specifies registers per chip (e.g.,
    here you would edit the `CFG_IREF` for the VFAT of interest). Within
    the `vfat3` directory there will be a set of configuration files and
    symlinks that are given by the following pattern:

    ``` bash
    lrwxrwxrwx    1 gemuser  1001            53 Aug 10 15:27 config_OHX_VFATY.txt -> /mnt/persistent/gemdaq/vfat3/config_OHX_VFATY_cal.txt
    -rw-r--r--    1 gemuser  1001          1267 Aug 10 15:27 config_OHX_VFATY_cal.txt

    The symlink is what is used by the configuration command to configure the ``vfat3`` in ``(ohN,vfatN) = (X,Y)`` position; so this must always be a valid link.
    An example of how this file is expected to look can be found in the section :ref:`gemos-frontend-vfat3-config-file-ctp7`.
    ```

-   `xml`

    The address table `xml` files will be found under the `xml` folder
    and a set of symlinks will be specified there, for example:

    ``` bash
    -rw-r--r--    1 51446    1399      21149299 Aug  8 15:46 gem_amc_top.pickle
    lrwxrwxrwx    1 51446    1399            18 Aug  8 15:45 gem_amc_top.xml -> gem_amc_v3_5_3.xml
    -rw-r--r--    1 51446    1399        136064 Aug  8 15:45 gem_amc_v3_5_3.xml
    -rw-r--r--    1 51446    1399        102472 Aug  8 15:41 oh_registers_3.1.2.B.xml
    lrwxrwxrwx    1 51446    1399            24 Aug  8 15:41 optohybrid_registers.xml -> oh_registers_3.1.2.B.xml
    ```

#### `/mnt/persistent/gemuser`

The `gemuser` subdirectory is the `$HOME` folder of the `gemuser` user.
Here you will be able to modify the environment that is set at login, as
well as add `ssh` keys for passwordless login as the `gemuser` user.

## The CTP7 Linux image

!!! warning
    If you are not a DAQ expert, you should not executing procedures listed
    on this page!

### Updating the CTP7 Linux image

1.  Login as `root` (this logs in at `/home/root` which is not on the
    `/mnt` partition),

2.  Execute `/sbin/reboot` to make sure there are no running processes
    or other active sessions (this would prevent a Linux update),

3.  Login as `root` again and mount the partitions as `rw`:

    ``` bash
    mount -o remount,rw /dev/mmcblk0p3 /mnt/persistent
    mount -o remount,rw /dev/mmcblk0p1 /mnt/image
    mount -o remount,rw /dev/mmcblk0p2 /mnt/image-persist
    ```

4.  Place the Linux image in the `root` `$HOME` directory (so that it is
    *not* on the `/mnt` partition),

5.  As `root` execute `image-update linuxImageFile.img`, an example
    *successful* output would look like

    ``` bash
    root@eagle26:~# image-update LinuxImage-CTP7-GENERIC-20180529T153916-0500-4935611.img
    Ensuring /mnt/persistent is writeable.
    *** Extracting Image ***

    *** Verifying Signature ***
    Verified OK

    *** Extracting Image Contents ***

    *** Running Installation Script ***

    Installing Image: CTP7-GENERIC-20180529T153916-0500-4935611
    Mounting image filesystem read-write
    Copying boot image
    Mounting image filesystem read-only
    Mounting image-persist filesystem read-write
    Installing stage tarball
    Installing documentation
    Syncing

    Update complete!
    Rebooting!

    Broadcast message from root@eagle26 (pts/0) (Thu Aug  9 08:31:00 2018):

    The system is going down for reboot NOW!
    ```

6.  Finally, login again (as any user) and check that the
    [build_id]{.title-ref} reflects the new image, for the above example
    it would have printed:

    ``` bash
    eagle26:~$ cat /mnt/image-persist/build_id
    CTP7-GENERIC-20180529T153916-0500-4935611
    ```

### Filesystem mounted as readonly

More recent Linux images of the CTP7 have mounted `/mnt/persistent/`
partition as `readonly`. This can be resolved by doing

``` bash
echo yes > /mnt/persistent/config/persistent_writeable
```

The next time the card boots the `/mnt/persistent` partition will be
mounted as writeable.

If the system is running and cannot be rebooted (e.g., during
data-taking) the following short cuts exist and can be executed by
non-`root` users:

``` bash
setpersistent rw # Sets partition writeable
setpersistent ro # Sets partition readonly
```

Some older versions of the Linux image do *not* feature these commands,
in this case, mount the partitions as `rw` via:

``` bash
mount -o remount,rw /dev/mmcblk0p3 /mnt/persistent
mount -o remount,rw /dev/mmcblk0p1 /mnt/image
mount -o remount,rw /dev/mmcblk0p2 /mnt/image-persist
```

### Logging (`syslogd`)

The system log of the CTP7 can be found here:

``` bash
/var/log/messages
```

!!! note
    This file is *not* found under the `/mnt/persistent` partition and is
    re-written everytime the card reboots. To ensure log information is not
    lost, the DAQ PC hosting the `sysmgr` is configured to receive the same
    log messages remotely. On that PC the log file is file found under:
    
    ``` bash
    /var/log/remote/<cardname>/messages.log
    ```
    
    Where `<cardname>` is the IP address or network alias of the CTP7 (e.g.,
    `eagle64`), and there will typically be two directories for each CTP7.
    This file is written to disk on the DAQ PC and will persist through
    crashes/reboots of the CTP7 (so no information will be lost).

You can see the most recent information found in the log file via:

``` bash
tail -25 /var/log/messages
```

This will display the last 25 lines in the log file on the CTP7
(executed as `gemuser` on the CTP7). Use the `tail` command similarly to
view the log on the DAQ PC.

#### Restarting `syslogd`

In some weird cases `syslogd` may not be running after the CTP7 boots.
This typically indicates that the CTP7 did not recieve a full
configuration from the `sysmgr`. The result is that the log file:

``` bash
/var/log/messages
```

will not being created. To resolve this, on the CTP7 execute (replace
`192.168.0.180` with the IP address of the DAQ PC on which the `sysmgr`
service is running):

``` bash
/sbin/syslogd -R 192.168.0.180 -L -s 1024 -b 8
```

and the process should again be running, e.g.,

``` bash
root@eagle63:~# ps l | grep syslog
S     0  9161     1  2792    64 0:0   13:31 00:00:00 /sbin/syslogd -R
192.168.0.180 -L -s 1024 -b 8
S     0  9163  9146  2796   288 pts4  13:32 00:00:00 grep syslog
```

## Reprogramming the CTP7 FPGA

All current generation electronics systems use AMC firmware version
`3.X.Y` or higher.

!!! important
    This procedure should **always** be done as `gemuser`

### FW reload (`cold_boot`)

To only reload the firmware (or load a newly installed firmware image),
a reduced `cold_boot` procedure can be followed. Depending on the
version of the hardware, the polarity of some of the links in the CXPs
may be inverted, thus choose the appropriate action based on your setup:

=== "GE1/1 v3"

    ``` bash
    cold_boot_invert_tx.sh
    ```

=== "GE2/1"

    ``` bash
    cold_boot_invert_cxp_rx.sh
    ```

=== "ME0"

    ``` bash
    cold_boot_invert_cxp_rx.sh
    ```

!!! important
    It is crucial to ensure that all `GTH Status` values (0 through 35)
    return `0x7`. If `0x6` is returned then you\'ll need to repeat the
    `cold_boot` procedure. If any other value is returned (e.g., `0x0`) the
    CTP7 may not be receiving a clock from the `AMC13` and you\'ll need to
    check that the AMC13 is configured correctly (see
    `userguide:gemos-amc13-enabling-clock`{.interpreted-text role="ref"}).

### Full recovery: `recover.sh`

If you need to perform a full recovery, e.g., after a reboot of the CTP7
or a power cut, simply execute:

``` bash
recover.sh
```

This script lives in the `/mnt/persistent/gemdaq/scripts` directory, and
upon execution will:

-   Reload the CTP7 FW
-   Start `ipbus`
-   Start `rpcsvc`
-   Load the OH FW into the CTP7 RAM for PROM-less, i.e., `BLASTER(tm)`,
    programming
-   Disable forwarding of TTC `HardReset` signals to the front-end

Again, it is critical to ensure that all `GTH Status` values (0 through
35) return `0x7`.

!!! warning
    Sometimes this will not enable `rpcsvc` or `ipbus` correctly. After
    calling `recover.sh` it is important to check if `rpcsvc` and `ipbus`
    are running on the card.

An example of a successful recovery is illustrated in this [elog
entry](http://cmsonline.cern.ch/cms-elog/1060543).

!!! note
    If you are calling this after the card has been rebooted or power cycled
    you should ensure the `texas` account is not the owner of the `rpcsvc`
    service. You should, as either `root` or `texas`, issue
    `killall rpcsvc`, and then logout and log back in as `gemuser` to issue
    the recover command.

## Register CLI advanced usage

In addition to the overview provided in the
`user guide<userguide:gemos-gem-reg-cli>`{.interpreted-text role="ref"},
the `gem_reg.py` tool allows you to perform additional actions on GEM
hardware:

The tool attempts to open an RPC connection to the specified host, thus
the `rpcsvc` service must be running as `gemuser` on that card.
Additionally, the tool parses the address table locally (on the calling
PC) by using the `pickle` file. If the pickle file and address table are
mismatched, errors can occur (this should not happen if updates are done
in the approved way).

An overview of the most common commands is found in the
`user guide<userguide:gemos-cli-commands>`{.interpreted-text
role="ref"}. All commands are documented in the
:py`xhal API<xhal:gem-reg>`{.interpreted-text role="mod"}. A few expert
actions are described here for reference.

## Update the OptoHybrid FW

The bitfile for the OptoHybrid FPGA is loaded into the RAM on the CTP7,
and from there it is loaded remotely to the connected OptoHybrids.

!!! important
    The following requires knowledge of the `root` password of the CTP7. If
    you do not know the `root` password you should not be executing this
    procedure. Please contact the sysadmin of your test stand and ask to
    have this done for you.

Most of the following steps are performed whenever the [setup_ctp7.sh
script\<gemctp7user:setup_ctp7>]{.title-ref} script is provided with the
`-o` option, indicating that firmware for the OptoHybrid should be
updated (see [:ref:gemos-ctp7-helper-scripts]{.title-ref}). The script
will **not** create an elog for you, nor will it automatically load the
updated OptoHybrid firmware into the CTP7 RAM or reprogram the connected
OptoHybrids.

### The full procedure

1.  Begin by creating an elog entry in the relevant elog explaining what
    you are about to do (e.g., for 904 Coffin or Integration stand use
    the `904 Integration` elog, for QC8/V3 Electronics R&D use the
    `Quality Control/DAQ Station` elog, for P5 use the `P5/DAQ` elog.)

2.  Login to the DAQ machine of interest (e.g., `gem904qc8daq` or
    `gem904daq01`)

3.  Navigate to the [OptoHybridv3 FW repository release
    page](https://github.com/cms-gem-daq-project/OptoHybridv3/releases)

4.  Select the release you are interested in

    !!! note
        GE1/1 OptoHybrid FW are labeled as `3.X.Y.(A|B|C|1C)` Very few
        `OHv3a` and `OHv3b` pieces still remain in use, so current firmware
        is only produced for long (`1C`) and short (`C`) GEB compatibility.
        GE2/1 OptoHybrid FW are labeled as `3.X.Y.2A` (yes, it is
        inconsistent, but they are also called OHv1 and OHv2\...)

    For the purpose of this description, we will assume a version of
    `3.2.8.1C`.

5.  Navigate to the firmware directory on the 904 NAS for OH FW:

    ``` bash
    ## GE1/1
    cd /data/bigdisk/GEMDAQ_Documentation/system/firmware/files/OptoHybrid/V3/OHv3(a|b|c)_firmware
    ## GE2/1
    cd /data/bigdisk/GEMDAQ_Documentation/system/firmware/files/OptoHybrid/GE2.1_Artix7
    ```

6.  Download the `OH_3.2.8.1C.tar.gz` file from the release to the NAS
    area on 904, do this via:

    ``` bash
    curl -LO  https://github.com/cms-gem-daq-project/OptoHybridv3/releases/download/3.2.8.X/OH_3.2.8.1C.tar.gz
    ```

7.  Unpack the archive by executing (this will create a subfolder
    `OH_3.2.8.1C`):

    ``` bash
    tar zxf OH_3.2.8.1C.tar.gz
    ```

8.  Navigate to the newly created subfolder:

    ``` bash
    cd OH_3.2.8.1C
    ```

9.  Create `*.bit` and `*.mcs` file symlinks:

    ``` bash
    ln -sf OH_3.2.8.1C.bit optohybrid_top.bit
    ln -sf OH_3.2.8.1C.mcs optohybrid_top.mcs
    ```

10. Upload the `*.bit` and `*.mcs` files and symlinks to the CTP7 (where
    XX is the serial number of the CTP7):

    ``` bash
    rsync -ach --progress --partial --links OH_*.{bit,mcs} root@eagleXX:/mnt/persistent/gemdaq/oh_fw
    ```

11. Create the `optohybrid_registers.xml` symlink:

    ``` bash
    ln -sf oh_registers_3.X.Y.xml optohybrid_registers.xml
    ```

12. Upload the the xml address table and symlink to the CTP7:

    ``` bash
    rsync -ach --progress --partial --links *.xml root@eagleXX:/mnt/persistent/gemdaq/xml
    ```

13. Login to the CTP7 of interest and load the new OH FW into the CTP7
    RAM:

    ``` bash
    ssh gemuser@eagleXX
    cd /mnt/persistent/gemdaq/gemloader
    ./gemloader_configure.sh
    ```

14. Update the `optohybrid_registers.xml` symlink on the DAQ machine:

    ``` bash
    ln -sf /data/bigdisk/GEMDAQ_Documentation/system/firmware/files/OptoHybrid/V3/OHv3c_firmware/OH_3.2.8.1C/oh_registers_3.2.8.1C.xml ${GEM_ADDRESS_TABLE_PATH}/optohybrid_registers.xml
    ```

15. Make sure the symlink you just created is valid

16. Update the `pickle` file following
    `these instructions<gemos-ctp7-update-pickle>`{.interpreted-text
    role="ref"}

17. If the address table has changed you must update the `LMDB` on the
    CTP7, see `gemos-ctp7-update-lmdb`{.interpreted-text role="ref"}.

18. To confirm that the update was successful, reprogram all OptoHybrids
    following the instructions under
    `gemos-optohybrid-programming`{.interpreted-text role="ref"}.

    !!! warning
        This will kill any running process on the hardware, but if you\'re
        updating FW no one should be using the system anyway.

19. Summarize the actions you took in the elog entry you have already
    started.
## Update the address table pickle file

For certain tools on the CTP7, the LMDB is not used to look up the
register to address mapping, and instead this is done with a \"pickled\"
version of the XML address table. When changing the firmware, in
addition to updating the XML address tables and the LMDB, the `pickle`
file itself must also be updated, in order to ensure consistency between
the three sources of information.

!!! note
    In the future, this will no longer be necessary, as there will be one
    single source of truth

All steps except for the final step are performed whenever the
[setup_ctp7.sh script\<gemctp7user:setup_ctp7>]{.title-ref} is provided
with *either* the `-o` or `-c` options, indicating that firmware for
either the OptoHybrid or CTP7 should be updated (see
[:ref:gemos-ctp7-helper-scripts]{.title-ref}). When using that script, a
message will be printed

``` bash
New pickle file has been copied to the CTP7, make sure you modify it correctly
```

which means that you should manually modify the pickle file *in situ* on
the CTP7.

### The full procedure

1.  On the DAQ machine delete the pickle file found under
    `$GEM_ADDRESS_TABLE_PATH`

    ``` bash
    rm ${GEM_ADDRESS_TABLE_PATH}/amc_address_table_top.pickle
    ```

2.  Create a new pickle file on the DAQ machine, to do this execute:
    `gem_reg.py`, this will automatically create a new pickle file under
    `$GEM_ADDRESS_TABLE_PATH`. Then exit the tool by typing `exit`.

3.  Upload the new pickle file to the CTP7:

    ``` bash
    scp ${GEM_ADDRESS_TABLE_PATH}/amc_address_table_top.pickle root@eagleXX:/mnt/persistent/gemdaq/xml/gem_amc_CTP7FW.3.A.B_OHFW3.X.Y.Z.pickle
    ```

    where 3.A.B is the CTP7 FW version and 3.X.Y.Z is the OH FW version.

4.  As `root` on the CTP7

    1.  Navigate to the xml address folder

    > ``` bash
    > cd /mnt/persistent/gemdaq/xml
    > ```

    1.  Open the pickle file with a text editor

    > ``` bash
    > vi gem_amc_CTP7FW.3.A.B_OHFW3.X.Y.{A|B|C}.pickle
    > ```

    1.  Replace the third line of the pickle file:

        ``` bash
        q^A]q^B(]q^C(U^GGEM_AMCq^D(creg_utils.reg_interface.common.reg_xml_parser
        ```

        to match:

        ``` bash
        q^A]q^B(]q^C(U^GGEM_AMCq^D(crw_reg
        ```

        !!! warning
            **Do not copy paste this, you \*must\* manually type it**. If
            you copy/paste you may insert a hidden control character that
            will cause the file to **not** be parsed correctly and any
        subsequent register access action will fail.

## Using the CTP7 helper scripts

In order to facilitate several common actions performed on the CTP7,
several helper scripts have been developed. In order to use them, you
should check out the `gemctp7user` repository:

``` bash
git clone https://github.com/cms-gem-daq-project/gemctp7user.git
```

To get help about the available options, execute:

``` bash
./setup_ctp7.sh -h
```

From the menu you can choose the appropriate action. Several common
actions are outlined below.

### Setting up a *new* CTP7

If you have *just* received a new CTP7, or a new SD card has been placed
in an CTP7 already in your possession, you will need to setup the linux
partition on the card. To do this, from the test stand\'s DAQ PC execute
the following:

1.  Execute the `setup_ctp7.sh` script from the `gemctp7user` repo:

    ``` bash
    cd gemctp7user
    ./setup_ctp7.sh -o X.Y.Z.Q -c A.B.C -l 12 -a -u eagleVV
    ```

This will place [OptoHybrid
firmware](https://github.com/cms-gem-daq-project/OptoHybridv3/releases)
version `X.Y.Z.Q`, [CTP7
firmware](https://github.com/cms-gem-daq-project/GEM_AMC/releases)
version `A.B.C`, create the `gemuser` account, obtain the latest
versions of `xhal` and `ctp7_modules`, and transfer all binaries/bit
files/xml\'s/etc., to the approrpriate locations on the CTP7.

!!! warning
    You may find that `rpcsvc` may not be running at the time that the
    `setup_ctp7.sh` script tries to update the LMDB. This will cause the
    automatic update of the LMDB to fail. This is okay, you can do it
    manually with the `gem_reg.py` CLI (you should ensure that the `pickle`
    file of the address table is correct):

``` bash
gem_reg.py -n ${ctp7host} -e update_lmdb /mnt/persistent/gemdaq/xml/gem_amc_top.xml
```

### Update CTP7 SW packages

To update only the software packages, e.g., `xhal` library,
`ctp7_modules`, or other libraries on the CTP7

1.  Execute the `setup_ctp7.sh` sxcript from the `gemctp7user` repo:

    ``` bash
    cd gemctp7user
    ./setup_ctp7.sh -u eagleVV
    ```

### Update OH FW files

To uptdate the OptoHybrid FW files, e.g., firmware image (bitfile), GBTx
generic configurations, and address tables

1.  Execute the `setup_ctp7.sh` sxcript from the `gemctp7user` repo:

    ``` bash
    cd gemctp7user
    ./setup_ctp7.sh -o X.Y.Z.Q eagleVV
    ```

### Update AMC FW files

To uptdate the AMC (CTP7) FW files, e.g., firmware image (bitfile) and
address tables

1.  Execute the `setup_ctp7.sh` sxcript from the `gemctp7user` repo:

    ``` bash
    cd gemctp7user
    ./setup_ctp7.sh -c A.B.C -l 12 eagleVV
    ```
