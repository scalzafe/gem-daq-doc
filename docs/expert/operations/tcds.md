# TCDS

The trigger control and distribution system (TCDS) is responsible for delivering the LHC clock to all subsystems, transporting the TTS states between subsystems and the central DAQ infrastructure, distributing trigger decisions (L1As), and sending TTC B-channel commands (B-Gos).

The [TCDS](https://twiki.cern.ch/twiki/bin/view/CMS/TCDS) system is composed of a number of components, all working together.

| Component name            | HW component | SW component  | Notes                            |
|---------------------------|--------------|---------------|----------------------------------|
| Central partition manager | CPM          | CPMController | Not controlled by GEMs           |
| Local partition manager   | LPM (iPM)    | LPMController |                                  |
| Integrated TTCci          | iCI          | iCIController | FW block inside the physical LPM |
| Partition interface       | PI           | PIController  |                                  |
