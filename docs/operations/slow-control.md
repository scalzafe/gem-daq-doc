# Common slow control actions

## Checking firmware versions

To get the firmware version of a CTP7 and all its programmed OptoHybrids
from `gem_reg.py` execute:

``` sh
kw RELEASE
```

## Checking CTP7 mapping register

One major difference between OHv3a and {OHv3b, OHv3c} is that OHv3a uses
a different set of e-links to communicate with the OptoHybrid FPGA on
GBT0. The mapping the CTP7 uses is hard coded in the FW but can be
toggled between OHv3a and {OHv3b, OHv3c} behavior. The default behavior
in recent CTP7 FW releases is {OHv3b, OHv3c} and this implies the
mapping register is `0x1`:

``` sh
eagle60 > kw MAPPING
0x66400044 rw   GEM_AMC.GEM_SYSTEM.VFAT3.USE_OH_V3B_MAPPING             0x00000001
```

Writing this register to `0x0` will switch to `OHv3a` e-link assignment.

## Checking trigger rates

To see some useful trigger information on the CTP7, in `gem_reg.py`
execute `kw GEM_AMC.TRIGGER.OHX` for the OptoHybrid of interest and
several registers displaying the rate, s-bit cluster info, and link
health registers (mentioned above) will be shown. Execute
`doc <register>` to learn more about each register.

To see the trigger rate each VFAT is sending on `OHX` execute in
`gem_reg.py`:

``` sh
kw GEM_AMC.OH.OHX.FPGA.TRIG.CNT.VFAT
```

## Getting info about the CTP7

If your CTP7 is in shelf `X` slot `Y` execute:

``` sh
amc_info_uhal.py --shelf=X -sY
```

This will print various info about the board, the DAQ link status, the
TTC status, and the SCA status.

!!! note
    `ipbus` must be running on the CTP7 for this tool to work

## Reading a register repeatedly

If you would like to repeatedly read the same register (e.g., in order
to determine the rate of bits being flipped) execute:

``` sh
repeated_reg_read.py REGISTER_NAME X Y --card eagleXX
```

This will read register `REGISTER_NAME` `X` times, pausing `Y`
microseconds between each read. Results are written to the terminal and
also an output text file: `[filename].txt`.

!!! tip
    `Y` should be set to \>= 250 microseconds.
