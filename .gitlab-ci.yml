stages:
  - test
  - deploy

variables:
  GIT_STRATEGY: fetch
  GIT_DEPTH: 0 # Git revision date plugin

# Builds the static website with MkDocs. The whole sequence of command is
# implemented into the before_script section for enhanced composability.
.build site:
  before_script:
    - python3 -m venv pyvenv
    - pyvenv/bin/pip install --upgrade pip
    - pyvenv/bin/pip install -r requirements.txt
    - pyvenv/bin/mkdocs build
  variables:
    PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  cache:
    paths:
      - .cache/pip
      - pyvenv
  artifacts:
    expire_in: 12h
    paths:
      - site

# Publish the website to an EOS directory specified in EOS_FULL_PATH via the
# SERVICE_ACCOUNT_USERNAME and SERVICE_ACCOUNT_PASSWORD credentials.
.deploy eos:
  script:
    - dnf install -y rsync
    - echo ${SERVICE_ACCOUNT_PASSWORD} | base64 -d | kinit -A -f ${SERVICE_ACCOUNT_USERNAME}@CERN.CH
    - >
      rsync -av --delete
      -e "ssh -o StrictHostKeyChecking=no -o GSSAPIAuthentication=yes -o GSSAPITrustDNS=yes -o GSSAPIDelegateCredentials=yes"
      site/ ${SERVICE_ACCOUNT_USERNAME}@lxplus.cern.ch:${EOS_FULL_PATH}
  after_script:
    - kdestroy

# Delete the website published to an EOS directory specified in EOS_FULL_PATH
# via the SERVICE_ACCOUNT_USERNAME and SERVICE_ACCOUNT_PASSWORD credentials.
.deploy eos:stop:
  script:
    - echo ${SERVICE_ACCOUNT_PASSWORD} | base64 -d | kinit -A -f ${SERVICE_ACCOUNT_USERNAME}@CERN.CH
    - >
      ssh -o StrictHostKeyChecking=no -o GSSAPIAuthentication=yes -o GSSAPITrustDNS=yes -o GSSAPIDelegateCredentials=yes
      ${SERVICE_ACCOUNT_USERNAME}@lxplus.cern.ch "rm -r ${EOS_FULL_PATH}"
  after_script:
    - kdestroy

# Run simple build check test job in a branch pipeline as long as a MR is not
# open. Once opened, run the job only manually upon user's request.
test:
  extends: .build site
  stage: test
  rules:
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
      when: manual
    - if: $CI_COMMIT_BRANCH
  script:
    - echo "Build done!"

# Prepare and publish a preview environment for each MR. Note that it may have
# to be triggered manually by the upstream maintainers.
deploy preview:
  extends:
    - .build site
    - .deploy eos
  stage: deploy
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
  environment:
    name: preview-$CI_MERGE_REQUEST_IID
    url: https://cmsgemonline.web.cern.ch/doc/preview-$CI_MERGE_REQUEST_IID
    on_stop: deploy preview:stop
  variables:
    EOS_FULL_PATH: ${EOS_SITE_PATH}/$CI_ENVIRONMENT_NAME/

# Stop the MR preview environment by making sure that the files are deleted
# from EOS.
deploy preview:stop:
  extends: .deploy eos:stop
  stage: deploy
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
      when: manual
      allow_failure: true
  environment:
    name: preview-$CI_MERGE_REQUEST_IID
    action: stop
  variables:
    GIT_STRATEGY: none
    EOS_FULL_PATH: ${EOS_SITE_PATH}/$CI_ENVIRONMENT_NAME/

# Publish the latest and greatest version in a well-known URL and environment.
deploy latest:
  extends:
    - .build site
    - .deploy eos
  stage: deploy
  rules:
    - if: $CI_PROJECT_NAMESPACE == 'cmsgemonline/gem-ops' && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  environment:
    name: latest
    url: https://cmsgemonline.web.cern.ch/doc/latest
  variables:
    EOS_FULL_PATH: ${EOS_SITE_PATH}/latest/
